import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  Image,
  TextInput,
  Linking,
} from 'react-native';

const ContactItem = props => {
  const {isNewUser, contact} = props.route.params;

  const [name, onChangeName] = useState(
    contact ? `${contact.givenName} ${contact.familyName}` : '',
  );
  const [phoneNumber, onChangePhoneNumber] = useState(
    contact ? contact.phoneNumbers[0].number : '',
  );
  const img =
    contact?.thumbnailPath ||
    'https://lh3.googleusercontent.com/proxy/37KwYSy2b3QzgiGrn5ADDH6j4x-rxPL2ifJiUN_zDAlbbVlB1TIZfRbA06DGSRRrhwL1LwK5rW_DaQVN_Tb8GA';

  const save = () => {
    if (name && phoneNumber) {
      const names = name.split(' ');
      const newContact = {
        phoneNumbers: [
          {
            label: 'mobile',
            number: phoneNumber,
          },
        ],
        givenName: names[0],
        familyName: names[1] || '',
      };

      props.navigation.navigate({
        name: 'Contacts',
        params: {newContact: newContact},
        merge: true,
      });
    }
  };

  const call = () => {
    Linking.openURL(`tel:${phoneNumber}`);
  };

  return (
    <View style={styles.container}>
      <Image style={styles.contactImage} source={{uri: img}} />
      <Text>Name</Text>

      {isNewUser ? (
        <TextInput
          style={styles.input}
          onChangeText={onChangeName}
          value={name}
        />
      ) : (
        <Text style={styles.text}>{name}</Text>
      )}
      <Text>Phone number</Text>
      {isNewUser ? (
        <TextInput
          style={styles.input}
          onChangeText={onChangePhoneNumber}
          value={phoneNumber}
        />
      ) : (
        <Text style={styles.text}>{phoneNumber}</Text>
      )}
      <Button
        title={isNewUser ? 'Save' : 'Call'}
        onPress={isNewUser ? save : call}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  contactImage: {
    width: 150,
    height: 150,
    alignSelf: 'center',
    marginBottom: 40,
  },
  input: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    marginBottom: 20,
    marginTop: 5,
    fontSize: 18,
  },
  text: {
    marginBottom: 25,
    marginTop: 15,
    fontSize: 18,
  },
});

export default ContactItem;
