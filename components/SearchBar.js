import React, {useState} from 'react';
import {
  View,
  TextInput,
  UIManager,
  LayoutAnimation,
  Animated,
  ActivityIndicator,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Text,
  StyleSheet,
} from 'react-native';

const SearchBar = props => {
  const [hasFocus, setHasFocus] = useState(false);
  const [isEmpty, setIsEmpty] = useState(true);
  const [loader, setShowLoader] = useState(false);

  let input;

  const focus = () => {
    input.focus();
  };

  const blur = () => {
    input.blur();
  };

  const clear = () => {
    input.clear();
    onChangeText('');
  };

  const cancel = () => {
    blur();
  };

  const showLoader = () => {
    setShowLoader(true);
  };

  const hideLoader = () => {
    setShowLoader(false);
  };

  const onFocus = () => {
    if (UIManager.configureNextLayoutAnimation) LayoutAnimation.easeInEaseOut();
    setHasFocus(true);
  };

  const onBlur = () => {
    if (UIManager.configureNextLayoutAnimation) LayoutAnimation.easeInEaseOut();
    setHasFocus(false);
  };

  const onChangeText = text => {
    props.onChangeText(text);
    setIsEmpty(text === '');
  };

  const {
    container,
    inputStyle,
    leftIconStyle,
    rightContainer,
    rightIconStyle,
    activityIndicator,
  } = styles;

  const inputStyleCollection = [inputStyle];
  if (hasFocus) inputStyleCollection.push({flex: 1});

  return (
    <TouchableWithoutFeedback onPress={focus}>
      <Animated.View style={container}>
        <View style={leftIconStyle}>
          <Text>🔍</Text>
        </View>
        <TextInput
          onFocus={onFocus}
          onBlur={onBlur}
          onChangeText={onChangeText}
          placeholder="Search..."
          style={inputStyleCollection}
          placeholderTextColor="#515151"
          autoCorrect={false}
          ref={ref => {
            input = ref;
          }}
        />
        <View style={rightContainer}>
          {hasFocus && loader ? (
            <ActivityIndicator
              key="loading"
              style={activityIndicator}
              color="#515151"
            />
          ) : (
            <View />
          )}
          {hasFocus && !isEmpty ? (
            <TouchableOpacity onPress={clear}>
              <View style={rightIconStyle}>
                <Text>ⅹ</Text>
              </View>
            </TouchableOpacity>
          ) : (
            <View />
          )}
        </View>
      </Animated.View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 40,
    borderRadius: 5,
    backgroundColor: '#DAE0E4',
    marginBottom: 5,
    marginTop: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputStyle: {
    alignSelf: 'center',
    marginLeft: 5,
    height: 40,
    fontSize: 14,
  },
  leftIconStyle: {
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 8,
  },
  rightContainer: {
    flexDirection: 'row',
  },
  rightIconStyle: {
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 8,
  },
  activityIndicator: {
    marginRight: 5,
  },
});

export default SearchBar;
