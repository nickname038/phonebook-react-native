import {PermissionsAndroid} from 'react-native';
import Contacts from 'react-native-contacts';

import React, {useState, useEffect} from 'react';
import {SafeAreaView, ScrollView, StyleSheet, Button} from 'react-native';
import SearchBar from './SearchBar';
import ListItem from './ListItem';
import Avatar from './Avatar';

const getAvatarInitials = textString => {
  if (!textString) return '';
  const text = textString.trim();
  const textSplit = text.split(' ');

  if (textSplit.length <= 1) return text.charAt(0);

  const initials =
    textSplit[0].charAt(0) + textSplit[textSplit.length - 1].charAt(0);

  return initials;
};

const ContactsList = props => {
  const [contacts, setContacts] = useState([]);
  const [prevContact, setPrevContact] = useState(null);
  const newContact = props.route?.params?.newContact;

  const loadContacts = () => {
    Contacts.getAll().then(contacts => {
      setContacts(contacts);
    });
  };

  useEffect(() => {
    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
      title: 'Contacts',
      message: 'This app would like to view your contacts.',
    }).then(() => {
      loadContacts();
    });
  }, []);

  if (newContact !== prevContact) {
    Contacts.addContact(newContact);
    setPrevContact(newContact);
    loadContacts();
  }

  const search = text => {
    if (!text) {
      loadContacts();
    } else if (!Number(text).isNaN) {
      Contacts.getContactsByPhoneNumber(text).then(contacts => {
        setContacts(contacts);
      });
    } else {
      Contacts.getContactsMatchingString(text).then(contacts => {
        setContacts(contacts);
      });
    }
  };

  const deleteContact = contact => {
    Contacts.deleteContact(contact).then(() => {
      loadContacts();
    });
  };

  return (
    <SafeAreaView style={styles.container}>
      <SearchBar onChangeText={search} />
      <Button
        title="Add new"
        onPress={() => {
          props.navigation.navigate('ContactItem', {isNewUser: true});
        }}
      />
      <ScrollView>
        {contacts.map(contact => {
          return (
            <ListItem
              leftElement={
                <Avatar
                  img={
                    contact.hasThumbnail
                      ? {uri: contact.thumbnailPath}
                      : undefined
                  }
                  placeholder={getAvatarInitials(
                    `${contact.givenName} ${contact.familyName}`,
                  )}
                  width={40}
                  height={40}
                />
              }
              key={contact.recordID}
              title={`${contact.givenName} ${contact.familyName}`}
              onPress={() => {
                props.navigation.navigate('ContactItem', {
                  isNewUser: false,
                  contact,
                });
              }}
              onDelete={() => deleteContact(contact)}
            />
          );
        })}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default ContactsList;
