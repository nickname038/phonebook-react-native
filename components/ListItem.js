import React from 'react';
import {View, TouchableHighlight, Text, StyleSheet, Button} from 'react-native';

const ListItem = props => {
  const {leftElement, title, onPress} = props;

  const {
    itemContainer,
    leftElementContainer,
    rightSectionContainer,
    mainTitleContainer,
    rightTextContainer,
    titleStyle,
  } = styles;

  return (
    <View>
      <TouchableHighlight onPress={onPress} underlayColor="#f2f3f5">
        <View style={itemContainer}>
          {leftElement ? (
            <View style={leftElementContainer}>{leftElement}</View>
          ) : (
            <View />
          )}
          <View style={rightSectionContainer}>
            <View style={mainTitleContainer}>
              <Text style={titleStyle}>{title}</Text>
            </View>
            <View style={rightTextContainer}>
              <Button title="Delete" onPress={props.onDelete} />
            </View>
          </View>
        </View>
      </TouchableHighlight>
    </View>
  );
};

const styles = StyleSheet.create({
  itemContainer: {
    flexDirection: 'row',
    minHeight: 44,
    height: 63,
  },
  leftElementContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 2,
    paddingLeft: 13,
  },
  rightSectionContainer: {
    marginLeft: 18,
    flexDirection: 'row',
    flex: 20,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: '#515151',
  },
  mainTitleContainer: {
    justifyContent: 'center',
    flexDirection: 'column',
    flex: 1,
  },
  rightTextContainer: {
    justifyContent: 'center',
    marginRight: 10,
  },
  titleStyle: {
    fontSize: 16,
  },
});

export default ListItem;
