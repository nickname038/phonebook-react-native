import React from 'react';
import {Image, View, Text, StyleSheet} from 'react-native';

const Avatar = props => {
  const renderImage = () => {
    const {img, width, height} = props;
    const {imageContainer, image} = styles;

    const viewStyle = [imageContainer];
    viewStyle.push({borderRadius: Math.round(width + height) / 2});
    return (
      <View style={viewStyle}>
        <Image style={image} source={img} />
      </View>
    );
  };

  const renderPlaceholder = () => {
    const {placeholder, width, height} = props;
    const {placeholderContainer, placeholderText} = styles;

    const viewStyle = [placeholderContainer];
    viewStyle.push({borderRadius: Math.round(width + height) / 2});

    return (
      <View style={viewStyle}>
        <View style={viewStyle}>
          <Text
            adjustsFontSizeToFit
            numberOfLines={1}
            minimumFontScale={0.01}
            style={[{fontSize: Math.round(width) / 2}, placeholderText]}>
            {placeholder}
          </Text>
        </View>
      </View>
    );
  };

  const {img, width, height} = props;
  const {container} = styles;
  return (
    <View style={[container, {width, height}]}>
      {img ? renderImage() : renderPlaceholder()}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
  },
  imageContainer: {
    overflow: 'hidden',
    justifyContent: 'center',
    height: '100%',
  },
  image: {
    flex: 1,
    alignSelf: 'stretch',
    width: undefined,
    height: undefined,
  },
  placeholderContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#dddddd',
    height: '100%',
  },
  placeholderText: {
    fontWeight: '700',
    color: '#ffffff',
  },
});

export default Avatar;
