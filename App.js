import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';

import ContactsList from './components/Contacts';
import ContactItem from './components/ContactItem';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
        initialRouteName="Contacts">
        <Stack.Screen name="Contacts" component={ContactsList} />
        <Stack.Screen name="ContactItem" component={ContactItem} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
